package  
{
	import flash.display.Sprite;
	import flash.events.Event;
	/**
	 * ...
	 * @author Sean Lavery
	 */
	public class Block extends Sprite
	{
		public var graphic:Enemy_gfx;
		private var counter:Number;
		private var counterRate:Number;
		private var oscilationDistance:Number;
		private var oscilator:Number;
		private var randomLevel:Random;
		
		
		//blockspeed
		private var bs:Number = .04;
		
		public function Block(randomLevel:Random) 
		{
			addEventListener(Event.ADDED_TO_STAGE, added);
			this.randomLevel = randomLevel;
			counter = randomLevel.nextNumber()*20;
			counterRate = randomLevel.nextNumber() < .5 ? (bs*.5 * randomLevel.nextNumber() + bs): -(bs*.5* randomLevel.nextNumber() + bs);
			oscilationDistance = 40*randomLevel.nextNumber()+340;
			oscilator = 0;
		}
		
		private function added(e:Event):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, added);
			graphic = new Enemy_gfx();
			addChild(graphic);
			graphic.maskclip.scaleY = randomLevel.nextNumber() * 1 + 2;
		}
		
		public function update():void {
			counter += counterRate;
			oscilator = Math.sin(counter) *  oscilationDistance;
			graphic.y = oscilator;
			
		}
	}
}