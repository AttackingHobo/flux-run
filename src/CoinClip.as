package  
{
	import flash.events.Event;
	/**
	 * ...
	 * @author Sean Lavery
	 */
	public class CoinClip extends Coin 
	{
		private var game:Game;
		
		private var counter:Number;
		private var counterRate:Number;
		private var oscilationDistance:Number;
		private var oscilator:Number;
		private var bs:Number = .08;
		private var randomLevel:Random;
		
		public function CoinClip(game:Game) 
		{
			this.game = game;
			
			randomLevel = game.randomLevel;
			counter = randomLevel.nextNumber()*20;
			counterRate = .09;
			oscilationDistance = 50*randomLevel.nextNumber()+100;
			oscilator = 0;
			
			addEventListener(Event.ADDED_TO_STAGE, added);
		}
		
		private function added(e:Event):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, added);
		}
		public function update():void {
			counter += counterRate;
			oscilator = Math.sin(counter) *  oscilationDistance;
			y = oscilator+320;
		}
		
	}

}