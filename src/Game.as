package  
{
	import flash.display.SpreadMethod;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.events.TouchEvent;
	import flash.media.Sound;
	import flash.net.URLRequest;
	
	/**
	 * ...
	 * @author Sean Lavery
	 */
	public class Game extends Sprite 
	{
		public var inputIsDown:Boolean;
		private var player:Player;
		private var background:background_gfx;
		private var currentLevel:int;
		private var score:int;
		private var levelExit:win_gfx;
		private var blockController:BlockController;
		private var hud:HUD_GFX;
		private var blocks:Array;
		public var randomLevel:Random;
		
		public function Game() 
		{
			addEventListener(Event.ADDED_TO_STAGE, added);
		}
		
		private function added(e:Event):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, added);
			addEventListener(Event.ENTER_FRAME, update);
			
			addEventListener(MouseEvent.MOUSE_DOWN, inputDown);
			addEventListener(TouchEvent.TOUCH_BEGIN, inputDown);
			addEventListener(MouseEvent.MOUSE_UP, inputUp);
			addEventListener(TouchEvent.TOUCH_END, inputUp);
			
			stage.addEventListener(KeyboardEvent.KEY_DOWN, inputDown);
			stage.addEventListener(KeyboardEvent.KEY_UP, inputUp);
			
			init();
			
			
		}
		
		private function inputDown(e:Event):void 
		{
			inputIsDown = true;
		}
		
		
		
		private function inputUp(e:Event):void 
		{
			inputIsDown = false;
		}
		
		
		
		private function init():void 
		{
			playMusic();
			resetVariables();
			addGameObjects();
			
			
		}
		
		private function playMusic():void 
		{
			music.play(0, 99999);
		}
		
		private function addGameObjects():void 
		{
			
			
			
			
			realbackground = new SolidBackground();
			addChild(realbackground);
			
			blockController = new BlockController();
			addChild(blockController);
			
			addChild(coinHolder);
			
			background = new background_gfx();
			
			background.stop();
			
			spawnExit();
			
			player = new Player(this);
			
			addChild(player);
			addChild(background);
			
			spawnBlocks();
			spawnHUD();
			
			initBlocks();
			
			spawnLevel();
			resetLevel();
			
		}
		
		private function spawnHUD():void 
		{
			hud = new HUD_GFX();
			addChild(hud);
		}
		
		private function spawnExit():void 
		{
			levelExit = new win_gfx();
			addChild(levelExit);
			levelExit.y = 640 / 2;
			levelExit.x = 900;
		}
		
		private function spawnBlocks():void 
		{
			
			
			
		}
		
		private function initBlocks():void 
		{
			
		}
		
		private function spawnLevel():void 
		{
			for (var i:int = 0; i < currentLevel*.25+2 ; i++) {
				spawnBlock(randomLevel.nextNumber()*650+150);
			}
			for (var c:int = 0; c < 3; c++ ) {
				spawnCoin(randomLevel.nextNumber() * 650 + 150);
			}
		}
		
		private function spawnCoin(nextNumber:Number):void 
		{
			var coin:CoinClip =  new CoinClip(this);
			
			coin.y = 640 / 2;
			coin.x = randomLevel.nextMinMax(200,700);
			coins.push(coin);
			coinHolder.addChild(coin);
		}
		
		private function spawnBlock(x:int):void 
		{
			var block:Block = new Block(randomLevel);
			blockController.addChild(block);
			block.y = 640 / 2;
			block.x = x;
			blocks.push(block);
		}
		private var currentSeed:int;
		private var originalSeed:int = 6969420;
		private var bonus:int;
		private var music:Sound = loadSound("music");
		private var pickup:Sound = loadSound("pickup");
		
		private var winSound:Sound = loadSound("winsound");
		private var dieSound:Sound = loadSound("diesound");
		private var realbackground:SolidBackground;
		private var highScore:int = 0;
		private var coins:Array;
		private var coinHolder:CoinHolder = new CoinHolder();
		private var coinsPoints:int = 200;
		
		private static function loadSound(path:String):Sound {
			var sound:Sound = new Sound();
			sound.addEventListener(Event.COMPLETE, onSoundLoadingComplete);
			sound.load( new URLRequest(path + ".mp3") );
			return sound;
		}
		
		private static function onSoundLoadingComplete(e:Event):void 
		{
			
		}
		
		
		private function resetVariables():void 
		{
			currentSeed = int(Math.random()*int.MAX_VALUE);
			randomLevel = new Random(currentSeed);
			score = 0;
			currentLevel = 1;
			blocks = [] ;
			coins = [] ;
		}
		
		
		private function update(e:Event):void 
		{
			player.update();
			if (player.hitTestObject(levelExit.hitbox)) {
				winLevel();
			}
			
			updateBlocks();
			updateCoins();
			updateHud();
		}
		
		
		private function updateHud():void 
		{
			bonus = Math.max(0, bonus *= .998);
			hud.score.text = "SCORE " +score;
			hud.bonus.text = bonus+"+";
			hud.high.text = "HIGH "+highScore;
			hud.LevelName.text = "Level: " + currentLevel;
		}
		
		private function winLevel():void 
		{
			currentLevel ++;
			
			score += bonus;
			highScore = Math.max(highScore, score);
			winSound.play();
			currentSeed = randomLevel.nextInt();
			resetLevel();
			
		}
		
		private function resetLevel():void 
		{
			bonus = 200 * currentLevel+2000;
			background.gotoAndStop(currentLevel);
			randomLevel = new Random(currentSeed);
		
			player.resetPlayer();
			removeAllBlocks();
			removeCoins();
			spawnLevel();
		}
		
		private function removeAllBlocks():void 
		{
			for (var i:int = blocks.length - 1; i >= 0; i--) {
				removeBlock(i);
			}
		}
		private function removeBlock(i:int):void  {
			var block:Block = blocks[i];
			blockController.removeChild(block);
			blocks.splice(i, 1);
		}private function removeCoin(i:int):void {
			var coin:CoinClip = coins[i];
			coinHolder.removeChild(coin);
			coins.splice(i, 1);
		}
		private function updateCoins():void {
			for (var i:int = coins.length - 1; i >= 0 ; i --) {
				var coin:CoinClip = coins[i];
				coin.update();
				if (coin.hitTestObject(player)) {
					pickup.play();
					score += coinsPoints;
					highScore = Math.max(highScore, score);
					removeCoin(i);
				}
			}
		}
		private function removeCoins():void {
			for (var i:int = coins.length - 1; i >= 0 ; i --) {
				var coin:CoinClip = coins[i];
				removeCoin(i);
			}
		}
		private function updateBlocks():void 
		{
			for (var i:int = blocks.length - 1; i >= 0; i--) {
				var block:Block = blocks[i];
				block.update();
				if (block.graphic.maskclip.hitTestObject(player)) {
					removeAllBlocks();
					removeCoins();
					resetVariables();
					resetLevel();
					dieSound.play();
					return;
				}
			}
		}
	}
}