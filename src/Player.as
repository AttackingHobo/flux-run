package  
{
	import flash.display.Sprite;
	import flash.events.Event;
	
	/**
	 * ...
	 * @author Sean Lavery
	 */
	public class Player extends Sprite 
	{
		private var game:Game;
		public var graphic:player_GFX;
		
		public function Player(game:Game) 
		{
			this.game = game;
			addEventListener(Event.ADDED_TO_STAGE, added);
			
		}
		
		public function update():void 
		{
			if (game.inputIsDown) {
				graphic.x += 10;
			}
		}
		
		private function added(e:Event):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, added);
			graphic = new player_GFX();
			addChild(graphic);
			
			resetPlayer();
			
			
		}
		
		public function resetPlayer():void 
		{
			graphic.y = 640 / 2;
			graphic.x = 20;
		}
		
	}

}